FROM docker.io/mediagis/nominatim:4.0

ENV PBF_URL="https://download.geofabrik.de/europe/monaco-latest.osm.pbf" \
    REPLICATION_URL="https://download.geofabrik.de/europe/monaco-updates/" 


# Allow user to update /etc/passwd to insert himself

RUN apt update && apt install sudo
RUN sudo chgrp -R 0 /nominatim && chmod -R a+rwx /nominatim
RUN sudo chgrp -R 0 /etc && chmod -R a+rwx /etc
RUN useradd -m -p $NOMINATIM_PASSWORD nominatim

EXPOSE 8080
